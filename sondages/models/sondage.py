from sondages.app import db, app, login_manager
from sqlalchemy.orm import with_polymorphic
from sqlalchemy.sql.expression import update
from flask.ext.login import UserMixin

#
# SONDAGE 
#

class Sondage(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String(100))
  description = db.Column(db.String(1000))

  def __init__(self, name, description):
    super(Sondage, self).__init__()
    self.name = name
    self.description = description

  def __repr__(self):
    return "<Sondage (%d) %s>" % (self.id, self.name)

  def toDict(self):
    allQuestions = [x.toDict() for x in self.questions]
    return { 
              'id': self.id,
              'name': self.name,
              'description' : self.description,
              'questions' : allQuestions
            }

def getAllSondages():
  return Sondage.query.all()

def getSondage(id):
  return Sondage.query.filter(Sondage.id==id).first()

def getSondageId(name, description):
  return Sondage.query.filter(Sondage.name==name, Sondage.description==description).first().id

def addSondage(sondage):
  name = sondage['name']
  description = sondage['description']
  s = Sondage(name = name, description = description)
  db.session.add(s)
  db.session.commit()

def updateSondage(id, updatedSondage):
  session = db.session()
  sondage = session.query(Sondage).filter_by(id=id).first()
  sondage.name = updatedSondage['name']
  sondage.description = updatedSondage['description']
  session.commit()

def deleteSondage(sondage):
  for question in sondage.questions:
    reponses = Reponse.query.filter(Reponse.idQ == question.id).all()
    for reponse in reponses:
      db.session.delete(reponse)
  db.session().delete(sondage)
  db.session().commit()
 
#
# QUESTION 
#   

class Question(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  title = db.Column(db.String(120))
  sondage_id = db.Column(db.Integer, db.ForeignKey('sondage.id'))
  sondage = db.relationship("Sondage", backref=db.backref("questions", lazy="dynamic", cascade="all, delete-orphan"))
  type = db.Column(db.String(50))

  __mapper_args__ = {
      'polymorphic_identity': 'question',
      'polymorphic_on': type
  }

  def __repr__(self):
    return "<Question (%d) %s>" % (self.id, self.title)

  def toDict(self):
    return { 
              'id': self.id,
              'sondage_id': self.sondage_id,
              'title': self.title,
              'type': self.type
            }

class SimpleQuestion(Question):
  __tablename__ = 'simplequestion'

  id = db.Column(db.Integer, db.ForeignKey('question.id'), primary_key=True)
  firstAlternative = db.Column(db.String(500))
  secondAlternative = db.Column(db.String(500))
  firstChecked = db.Column(db.Boolean)

  __mapper_args__ = {
      'polymorphic_identity':'simplequestion'
  }

  def toDict(self):
    return { 
              'id': self.id,
              'sondage_id': self.sondage_id,
              'title': self.title, 
              'firstAlternative': self.firstAlternative, 
              'secondAlternative': self.secondAlternative,
              'firstChecked': self.firstChecked,
              'type': self.type
            }

def getAllQuestions():
  return db.session.query(Question).with_polymorphic(SimpleQuestion).all()

def addQuestion(question):
  id = question['sondage_id']
  title = question['title']
  firstAlternative = question['firstAlternative']
  secondAlternative = question['secondAlternative']
  firstChecked = question['firstChecked']
  if firstAlternative:
    q = SimpleQuestion(title=title, firstAlternative=firstAlternative, secondAlternative=secondAlternative, sondage_id=id, firstChecked=firstChecked)
  else:
    q = Question(title=title, sondage_id=id)
  db.session.add(q)
  db.session.commit()

def getQuestionsSlice(debut = 0, nombre = 10):
  return db.session.query(Question).with_polymorphic(SimpleQuestion).offset(debut).limit(nombre).all()

def getQuestion(id):
  return db.session.query(Question).with_polymorphic(SimpleQuestion).filter(Question.id==id).first()

def updateQuestion(id, sondage_id, title, firstAlternative="", secondAlternative="", firstChecked=False):
  session = db.session()
  question = session.query(Question).with_polymorphic(SimpleQuestion).filter_by(id=id).first()
  question.title = title
  question.sondage_id = sondage_id
  if firstAlternative:
    question.firstAlternative = firstAlternative
    question.secondAlternative = secondAlternative
    question.firstChecked = firstChecked
  session.commit()

def deleteQuestion(question):
  db.session().delete(question)
  db.session().commit()

#
# REPONSE 
#

class Reponse(db.Model):
  idQ = db.Column(db.Integer, primary_key=True)
  userPseudo = db.Column(db.String(100), primary_key=True)
  reponse = db.Column(db.String(1000))


  def toDict(self):
    return { 
              'question_id': self.idQ,
              'username': self.userPseudo,
              'answer' : self.reponse
            }
            
def getAllAnswers():
  return Reponse.query.all()

def getReponseForQuestion(idQ):
  return Reponse.query.filter(Reponse.idQ == idQ).all()

def getReponseForUser(pseudo):
  return Reponse.query.filter(Reponse.userPseudo == pseudo).all()

def getAnswer(idQ, pseudo):
  return Reponse.query.filter(Reponse.idQ == idQ, Reponse.userPseudo == pseudo).first()

def addAnswer(answer):
  idQ = answer['question_id']
  username = answer['username']
  answer = answer['answer']
  a = Reponse(idQ = idQ, userPseudo = username, reponse = answer)
  db.session.add(a)
  db.session.commit()