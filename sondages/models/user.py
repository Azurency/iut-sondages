from sondages.app import db, app, login_manager
from flask.ext.login import UserMixin
from flask.ext.wtf import Form
from wtforms import StringField, PasswordField, HiddenField, IntegerField
from wtforms.validators import DataRequired
from hashlib import sha256
from flask.ext.login import login_user

class User(db.Model, UserMixin):
  pseudo = db.Column(db.String(100), primary_key=True)
  mdp    = db.Column(db.String(100))
  droit  = db.Column(db.Integer)

  def get_id(self):
    return self.pseudo

class LoginForm(Form):
  pseudo = StringField('Pseudo', validators=[DataRequired()])
  mdp = PasswordField('Mot de passe', validators=[DataRequired()])
  next = HiddenField()

  def get_authenticated_user(self):
    user = User.query.get(self.pseudo.data)
    if user is None:
      return None
    m = sha256()
    m.update(self.mdp.data.encode())
    passwd = m.hexdigest()
    return user if passwd == user.mdp else None

class InscriptionForm(Form):
  pseudo = StringField('Pseudo', validators=[DataRequired()])
  mdp = PasswordField('Mot de passe', validators=[DataRequired()])

  def inscription(self):
    if User.query.get(self.pseudo.data) is None:
      newUser(self.pseudo.data, self.mdp.data, 0)
      return True
    return False

def newUser(pseudo, mdp, droit):
  from hashlib import sha256
  m = sha256()
  m.update(mdp.encode())
  u = User(pseudo=pseudo, mdp=m.hexdigest(), droit=droit)
  db.session.add(u)
  db.session.commit()

def get_all_user():
  return User.query.filter(User.droit == 0).order_by(User.pseudo.asc()).all()

def get_user_by_username(username):
  return User.query.get(username)

def userExist(username):
  return User.query.filter_by(pseudo = username) is not None

@login_manager.user_loader
def load_user(pseudo):
    return User.query.get(pseudo)