from .app import manager, db
from sondages.models.sondage import *
from sondages.models.user import *
import json

@manager.command
def loaddb(filename):
  '''
  Cree les tables et les remplit avec 
  les données du fichier json
  '''

  # Création de toutes les tables
  db.create_all()

  with open(filename) as json_file:
    sondage = json.load(json_file)

  sond = Sondage(name=sondage["name"], description=sondage["description"])
  db.session.add(sond)
  db.session.commit()

  for q in sondage["questions"]:
    if "firstAlternative" in q:
      oq = SimpleQuestion(title=q["title"], firstAlternative=q["firstAlternative"], secondAlternative=q["secondAlternative"], sondage_id=sond.id, firstChecked=q["firstChecked"])
    else:
      oq = Question(title=q["title"], sondage_id=sond.id)
    db.session.add(oq)
  db.session.commit()

@manager.command
def createAdmin(username,password):
  '''Create a new admin User'''
  newUser(username, password, 1)