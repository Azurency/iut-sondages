#! /usr/bin/env python3
from flask import Flask
app = Flask(__name__)
app.debug = True

from flask.ext.script import Manager
manager = Manager(app)

import os.path
def mkpath(p):
	return os.path.normpath(
		os.path.join(
			os.path.dirname(__file__),
			p))

from flask.ext.sqlalchemy import SQLAlchemy
app.config['SQLALCHEMY_DATABASE_URI'] = ('sqlite:///' + mkpath('../quest.db'))
db = SQLAlchemy(app)

from flask.ext.login import LoginManager
login_manager = LoginManager(app)

app.secret_key = '5S75L4nhFy9TF098tbfH'

app.config['SECRET_KEY'] = "24D99921-6556-45C3-AC7C-7821E5FBD238"

login_manager.login_view = "frontend.login"

from sondages.views import api
from sondages.views import frontend
app.register_blueprint(api.api)
app.register_blueprint(frontend.frontend)