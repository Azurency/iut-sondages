from sondages.app import app
from flask import Blueprint, render_template, jsonify, request, abort, make_response, url_for, redirect
from sondages.models.sondage import *
from sondages.models.user import *
from flask.ext.login import logout_user, login_required, current_user

api = Blueprint('api', __name__, url_prefix='/api/v1.0')

@api.errorhandler(404)
def not_found(error):
  return make_response(jsonify({'error': 'Not found'}), 404)

@api.errorhandler(403)
def not_found(error):
  return make_response(jsonify({'error': 'Forbidden'}), 403)

@api.errorhandler(405)
def not_found(error):
  return make_response(jsonify({'error': 'Method not allowed'}), 405)

@api.errorhandler(400)
def not_found(error):
  return make_response(jsonify({'error': 'Bad Request'}), 400)

def make_public_question(question):
  new_question = {}
  for field in question:
      if field == 'id':
          new_question['uri'] = url_for('api.get_question', id=question['id'], _external=True)
      elif field == 'sondage_id':
          new_question['sondage_uri'] = url_for('api.get_sondage', id=question['sondage_id'], _external=True)
      else:
          new_question[field] = question[field]
  return new_question

def make_public_sondage(sondage):
  new_sondage = {}
  for field in sondage:
      if field == 'id':
          new_sondage['sondage_uri'] = url_for('api.get_sondage', id=sondage['id'], _external=True)
      elif field == 'questions':
          new_sondage['questions'] = [make_public_question(x) for x in sondage['questions']]
      else:
          new_sondage[field] = sondage[field]
  new_sondage['id'] = getSondageId(new_sondage['name'],new_sondage['description'])
  return new_sondage

@api.route("/questions", methods=['GET'])
def get_questions():
  return jsonify({ 'questions': [make_public_question(x.toDict()) for x in getAllQuestions()] })

@api.route("/sondages", methods=['GET'])
def get_sondages():
  return jsonify({ 'sondages': [make_public_sondage(x.toDict()) for x in getAllSondages()] })

@api.route("/questions", methods=['POST'])
def create_question():
  if not request.json or not 'title' in request.json or not 'sondage_id' in request.json:
    abort(400)
  question = {
    'sondage_id': request.json['sondage_id'],
    'title': request.json['title'],
    'firstAlternative': request.json.get('firstAlternative', ""),
    'secondAlternative': request.json.get('secondAlternative', ""),
    'firstChecked': request.json.get('firstChecked', False)
  }
  addQuestion(question)
  return jsonify({ 'question': make_public_question(question) }), 201

@api.route("/sondages", methods=['POST'])
def create_sondage():
  if not request.json or not 'name' or not 'description' in request.json:
    abort(400)
  sondage = {
    'name': request.json['name'],
    'description': request.json['description']
  }
  addSondage(sondage)
  return jsonify({ 'sondage': make_public_sondage(sondage) }), 201

@api.route("/questions/<int:id>", methods=['GET'])
def get_question(id):
  question = getQuestion(id)
  if not question:
    abort(404)
  return jsonify({'question': make_public_question(question.toDict())})

@api.route("/sondages/<int:id>", methods=['GET'])
def get_sondage(id):
  sondage = getSondage(id)
  if not sondage:
    abort(404)
  return jsonify({'sondage': make_public_sondage(sondage.toDict())})

@api.route("/sondages/<int:id>", methods=['PUT'])
def update_sondage(id):
  sondage = getSondage(id)
  if not sondage:
    abort(404)
  if not request.json or not 'name' in request.json or not 'description' in request.json:
    abort(400)
  updatedSondage = {
    'name': request.json['name'],
    'description': request.json['description']
  }
  updateSondage(id, updatedSondage)
  return jsonify({'sondage': updatedSondage})

@api.route("/questions/<int:id>", methods=['PUT'])
def update_question(id):
  question = getQuestion(id)
  if not question:
    abort(404)
  if not request.json:
    abort(400)
  if not 'sondage_id' in request.json or not 'title' in request.json:
    abort(400)
  if question.type == 'simplequestion':
    if not 'firstAlternative' in request.json:
      abort(400)
    if not 'secondAlternative' in request.json:
      abort(400)
    if not 'firstChecked' in request.json:
      abort(400)
  if 'sondage_id' in request.json and type(request.json['sondage_id']) is not int:
    abort(400)
  if 'firstChecked' in request.json and type(request.json['firstChecked']) is not bool:
    abort(400)
  if question.type == "simplequestion":
    updatedQuestion = {
      'sondage_id': request.json.get('sondage_id', question.sondage_id),
      'title': request.json.get('title', question.title),
      'firstAlternative': request.json.get('firstAlternative', question.firstAlternative),
      'secondAlternative': request.json.get('secondAlternative', question.secondAlternative),
      'firstChecked': request.json.get('firstChecked', question.firstChecked)
    }
    updateQuestion(id, updatedQuestion['sondage_id'], updatedQuestion['title'], updatedQuestion['firstAlternative'], updatedQuestion['secondAlternative'], updatedQuestion['firstChecked'])
  else:
    updatedQuestion = {
      'sondage_id': request.json.get('sondage_id', question.sondage_id),
      'title': request.json.get('title', question.title)
    }
    updateQuestion(id, updatedQuestion['sondage_id'], updatedQuestion['title'])
  return jsonify({'question': make_public_question(updatedQuestion)}), 200

@api.route("/questions/<int:id>", methods=['PATCH'])
def patch_question(id):
  question = getQuestion(id)
  if not question:
    abort(404)
  if not request.json:
    abort(400)
  if 'sondage_id' in request.json and type(request.json['sondage_id']) is not int:
    abort(400)
  if 'firstChecked' in request.json and type(request.json['firstChecked']) is not bool:
    abort(400)
  if question.type == "simplequestion":
    updatedQuestion = {
      'sondage_id': request.json.get('sondage_id', question.sondage_id),
      'title': request.json.get('title', question.title),
      'firstAlternative': request.json.get('firstAlternative', question.firstAlternative),
      'secondAlternative': request.json.get('secondAlternative', question.secondAlternative),
      'firstChecked': request.json.get('firstChecked', question.firstChecked)
    }
    updateQuestion(id, updatedQuestion['sondage_id'], updatedQuestion['title'], updatedQuestion['firstAlternative'], updatedQuestion['secondAlternative'], updatedQuestion['firstChecked'])
  else:
    updatedQuestion = {
      'sondage_id': request.json.get('sondage_id', question.sondage_id),
      'title': request.json.get('title', question.title)
    }
    updateQuestion(id, updatedQuestion['sondage_id'], updatedQuestion['title'])
  return jsonify({'question': make_public_question(updatedQuestion)}), 200

@api.route("/questions/<int:id>", methods=['DELETE'])
def delete_question(id):
  question = getQuestion(id)
  if not question:
    abort(404)
  deleteQuestion(question)
  return jsonify({'result': True}), 200

@api.route("/sondages/<int:id>", methods=['DELETE'])
def delete_sondage(id):
  sondage = getSondage(id)
  if not sondage:
    abort(404)
  deleteSondage(sondage)
  return jsonify({'result': True}), 200

@api.route("/users", methods=['POST'])
def create_user():
  username = request.json.get('username')
  password = request.json.get('password')
  if not username or not password:
    abort(400)
  if userExist(username):
    abort(400)
  newUser(username, password, 0)
  return jsonify({ 'username': user.pseudo }), 201, {'Location': url_for('api.get_user', id = user.id, _external = True)}

@api.route('/users/<string:username>', methods=['GET'])
def get_user(username):
  user = get_user_by_username(username)
  if not user:
      abort(404)
  return jsonify({'username': user.pseudo})

@api.route('/answers', methods=['POST'])
def create_answer():
  if not request.json or not 'question_id' in request.json or not 'username' in request.json or not 'answer' in request.json:
    abort(400)
  answer = getAnswer(request.json['question_id'], request.json['username'])
  if answer:
    abort(403)
  answer = {
    'question_id': request.json['question_id'],
    'username': request.json['username'],
    'answer': request.json['answer']
  }
  addAnswer(answer)
  return jsonify({ 'answer': answer }), 201

@api.route('/answers/<int:idQ>', methods=['GET'])
def get_answers_by_id(idQ):
  answer = getReponseForQuestion(idQ)
  if not answer:
    abort(404)
  return jsonify({'answers': [x.toDict() for x in answer]})

@api.route('/answers/<string:username>', methods=['GET'])
def get_answers_by_username(username):
  answer = getReponseForUser(username)
  if not answer:
    abort(404)
  return jsonify({'answers': [x.toDict() for x in answer]}) 