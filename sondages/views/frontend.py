from sondages.app import app
from flask import Blueprint, render_template, jsonify, request, abort, make_response, url_for, redirect
from sondages.models.sondage import *
from sondages.models.user import *
from flask.ext.login import logout_user, login_required, current_user

frontend = Blueprint('frontend', __name__)

@frontend.route("/")
def home():
  if not current_user.is_authenticated():
    return render_template("home.html", title="Accueil")
  return render_template("sondages.html", title="Questions")

@frontend.route("/statistiques")
@login_required
def statistiques():
  return render_template("statistiques.html", title="Statistiques")

@frontend.route("/questions/")
@frontend.route("/questions/<int:debut>/")
@frontend.route("/questions/<int:debut>/<int:nombre>")
def questions(debut=0, nombre=10):
  return render_template(
    "flex.html",
    title="Questions",
    questions=getQuestionsSlice(debut, nombre))

@frontend.route("/login/", methods=("GET","POST",))
def login():
  f = LoginForm()
  if not f.is_submitted():
    f.next.data = request.args.get('next')
  elif f.validate_on_submit():
    user = f.get_authenticated_user()
    if user:
      login_user(user)
      next = f.next.data or url_for("frontend.home")
      return redirect(next)
    else :
      return render_template("login.html", 
                         form=f,
                         erreur=True)
  else :
    return render_template("login.html", 
                         form=f,
                         erreur=True)
  return render_template("login.html", 
                         form=f)

@frontend.route("/logout/")
@login_required
def logout():
  logout_user()
  return redirect(url_for("frontend.home"))

@frontend.route("/inscription/", methods=("GET","POST"))
def inscription():
  f = InscriptionForm()
  if not f.is_submitted():
    if current_user.is_authenticated():
      return redirect(url_for("frontend.home"))
  elif f.validate_on_submit():
    check = f.inscription()
    if check:
      return redirect(url_for("frontend.login"))
    else:
      return render_template("inscription.html",
                          title="Inscription",
                          form=f,
                          erreur=True)
  else :
    return render_template("inscription.html",
                          title="Inscription",
                          form=f,
                          erreur=True)
  return render_template("inscription.html",
                          title="Inscription",
                          form=f)

