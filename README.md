# Projet iut-sondages
### Dans le cadres des études à l'IUT informatique d'Orléans

Projet réaliser par groupes de 2. Antoine Lassier et Sébastien Gedeon. Rendu le _01 avril 2015_.

## Sujet

Le but est de réaliser une application de gestion de sondages en utilisant une architecture REST et un client riche en JavaScript.
L'application doit essentiellement être de type **one page** (au moins pour la partie gestion des questions et visualisation des statistiques associées à un sondage).

Fonctionalitées attendues :
+ La création d'un sondage avec sa liste de questions
+ Des questions de type `SimpleQuestion`, `YesNoQuestion` ou `MultiplesAlternativesQuestion`
+ L'ajout d'une question ou d'un sondage utilisera des technologies de type _ajax_
+ Il faut prévoir des routes, vues et templates pour passer les sondages
+ Des route sont ajoutées dans l'API pour lire les résultats des sondages
+ L'application doit permettre de visualiser les résultats sous forme graphique (camenberts, histogrammes, etc.)

Extensions possibles :
+ Gérer les utilisateurs qui seront créateurs de sondages et leur authentification (sur l'API REST aussi)
+ Ajouter d'autre types de questions (réponses en texte ou numériques par exemple) et des fonctionnalités commes des plages d'ouverture des sondages

## Quickstart

Il est recommandé de créer un environnement virtuel Python pour faciliter le développement :

```bash
virtualenv venv
source venv/bin/activate
```


Le fichier `requirements.txt` contient toutes les dépendances requises par le projet, pour les installer, il faut utiliser la commande :

```bash
pip install -r requirements.txt
```


Il faut ensuite créer la base de données, un fichier `sondage/sondage1.json` est fourni comme base. Pour le charger il faut utiliser la commande :

```bash
./manage.py loaddb sondages/sondage1.json
```


Vous pouvez ensuite lancer le projet grâce à la commande

```bash
./manage.py runserver
```


Voici toutes les commandes de `manage.py` :

| Commande    | Descritption |
| ----------- | ------------ |
| runserver   | Lance un serveur de développement pour accéder au projet |
| loaddb      | Crée les tables dans un fichier `quest.db` depuis un fichier json passé en paramètre |
| shell       | Lance un shell dans le contexte de l'application |
| createAdmin | Crée un nouvel utilisateur admin avec le pseudo et le mdp passé en paramètre |

## API REST

Le serveur dipose actuellement de l'architecture REST suivante :

| Methode HTTP | URI | Action |
| -----------  | --- | ------ |
| GET | http://[hostname]/api/v1.0/questions | retourne la liste des questions |
| GET | http://[hostname]/api/v1.0/questions/[id] | retourne une question |
| POST | http://[hostname]/api/v1.0/questions | créer une nouvelle question |
| PUT | http://[hostname]/api/v1.0/questions/[id] | met à jour une question |
| DELETE | http://[hostname]/api/v1.0/questions/[id] | supprime une question |
| GET | http://[hostname]/api/v1.0/sondages | retourne la liste des sondages |
| POST | http://[hostname]/api/v1.0/sondages | créer un nouveau sondage |
| PUT | http://[hostname]/api/v1.0/sondages/[id] | met à jour un sondage |
| DELETE | http://[hostname]/api/v1.0/sondages/[id] | supprimer un sondage et ses questions |
| GET | http://[hostname]/api/v1.0/users/[username] | retourne un utilisateur |
| POST | http://[hostname]/api/v1.0/users | créer un nouvel utilisateur |
| GET | http://[hostname]/api/v1.0/answers/[username] | retourne les réponses d'un utilisateur |
| GET | http://[hostname]/api/v1.0/answers/[id] | retourne les réponses d'une question |
| POST | http://[hostname]/api/v1.0/answers | créer une nouvelle réponse |

## Tests avec CURL

Voici quelques exemples qui permettent de tester l'API avec `CURL`.

Ajouter une question :
```bash
curl -i -H "Content-Type: application/json" -X POST -d '{ "sondage_id": 1, "title": "on ajoute une question", "firstAlternative": "avec une API REST", "secondAlternative": "à la main dans la BD", "firstChecked": true}' http://127.0.0.1:5000/api/v1.0/questions
```

Mettre à jour une question (il faut fournir tout les éléments de la question, on la remplace):
```bash
curl -i -H "Content-Type: application/json" -X PUT -d '{ "title": "mise à jour du titre PUT", "sondage_id": 1 }' http://127.0.0.1:5000/api/v1.0/questions/2
```

Patcher une question (on peut ne fournir que certains éléments de la question pour la mettre à jour): 
```bash
curl -i -H "ContentType: application/json" -X PATCH -d '{ "title": "mise à jour du titre PATCH" }' http://127.0.0.1:5000/api/v1.0/questions/3
```

Supprimer une question :
```bash
curl -i -H "Content-Type: application/json" -X DELETE http://127.0.0.1:5000/api/v1.0/questions/2
```

Ajouter un utilisateur : 
```bash
curl -i -H "Content-Type: application/json" -X POST -d '{"username":"sondage","password":"question"}' http://127.0.0.1:5000/api/v1.0/users
```

Ajouter d'une réponse : 
```bash
curl -i -H "Content-Type: application/json" -X POST -d '{"question_id":2, "username":"admin", "answer": "reponse"}' http://127.0.0.1:5000/api/v1.0/answers
```

Ajouter un sondage :
```bash
curl -i -H "Content-Type: application/json" -X POST -d '{"name": "un super sondage", "description": "La description qui correpond à un super sondage."}' http://127.0.0.1:5000/api/v1.0/sondages
```

Modifier un sondage :
```bash
curl -i -H "Content-Type: application/json" -X PUT -d '{"name": "un super sondage modifié", "description": "La description qui correpond à un super sondage modifié."}' http://127.0.0.1:5000/api/v1.0/sondages/2
```

Supprimer un sondage :
```bash
curl -i -H "Content-Type: application/json" -X DELETE http://127.0.0.1:5000/api/v1.0/sondages/2
```